package com.example.moutaz.movieapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class MovieActivity extends ActionBarActivity {

    private ImageView poster;
    private TextView title;
    private TextView year;
    private TextView rate;
    private TextView overview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        initialize();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Intent intent = getIntent();
        title.setText(intent.getStringExtra("TITLE"));
        year.setText(intent.getStringExtra("YEAR").substring(0, 4));
        rate.setText(intent.getStringExtra("RATE") + "/10");
        overview.setText(intent.getStringExtra("OVERVIEW"));

        String imageLink = "http://image.tmdb.org/t/p/w185/" + intent.getStringExtra("IMAGE");
        Picasso.with(this)
                .load(imageLink)
                .into(poster);

    }

    private void initialize() {
        poster = (ImageView) findViewById(R.id.ivPoster);
        title = (TextView) findViewById(R.id.tvTitle);
        year = (TextView) findViewById(R.id.tvYear);
        rate = (TextView) findViewById(R.id.tvRate);
        overview = (TextView) findViewById(R.id.tvOverview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
