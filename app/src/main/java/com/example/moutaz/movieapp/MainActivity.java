package com.example.moutaz.movieapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private GridView gridView;
    private ProgressBar mProgressBar;
    private String lastStatus;
    private GridViewCustomAdapter gridViewAdapter;
    private ArrayList<GridItem> gridItemArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        gridItemArrayList = new ArrayList<>();

        lastStatus = getString(R.string.popular_tag);
        gridViewAdapter = new GridViewCustomAdapter(this, R.layout.grid_item_layout, gridItemArrayList);
        gridView.setAdapter(gridViewAdapter);

        new FetchData().execute();
        mProgressBar.setVisibility(View.VISIBLE);


        //listener
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                GridItem gridItem = (GridItem) parent.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), MovieActivity.class);

                intent.putExtra("TITLE", gridItem.getOrigTitle());
                intent.putExtra("OVERVIEW", gridItem.getOverview());
                intent.putExtra("RATE", gridItem.getVoteAvg());
                intent.putExtra("YEAR", gridItem.getRelDate());
                intent.putExtra("IMAGE", gridItem.getImage());

                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.popularity:
                if (item.isChecked())
                    item.setChecked(false);
                else
                    item.setChecked(true);
                if(!(lastStatus.equals(getString(R.string.popular_tag)))) {
                    lastStatus = getString(R.string.popular_tag);
                    new FetchData().execute();
                    mProgressBar.setVisibility(View.VISIBLE);

                }
                return true;
            case R.id.rated:
                if (item.isChecked())
                    item.setChecked(false);
                else
                    item.setChecked(true);
                if(!(lastStatus.equals(getString(R.string.rating_tag)))) {
                    lastStatus = getString(R.string.rating_tag);
                    new FetchData().execute();
                    mProgressBar.setVisibility(View.VISIBLE);

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public class FetchData extends AsyncTask<Void, Void, Void> {

        private final String LOG_TAG = FetchData.class.getSimpleName();

        private Void putDataInArrays(String JsonStr)throws JSONException {


            final String TITLE = "original_title";
            final String OVERVIEW = "overview";
            final String VOTE_AVG = "vote_average";
            final String REL_DATE = "release_date";
            final String IMAGES = "poster_path";

            JSONObject MJson = new JSONObject(JsonStr);
            JSONArray movieArray = MJson.getJSONArray("results");



            if(gridItemArrayList.size() == 20){
                gridItemArrayList.clear();
            }
            for(int i = 0; i < movieArray.length(); i++) {


                JSONObject movie = movieArray.getJSONObject(i);

                GridItem gridItem = new GridItem();
                gridItem.setOrigTitle(movie.getString(TITLE));
                gridItem.setOverview(movie.getString(OVERVIEW));
                gridItem.setVoteAvg(movie.getString(VOTE_AVG));
                gridItem.setRelDate(movie.getString(REL_DATE));
                gridItem.setImage(movie.getString(IMAGES));

                gridItemArrayList.add(gridItem);

            }

            return null;
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            String JsonStr = null;


            try {

                String link;
                if(lastStatus.equals(getString(R.string.popular_tag)))
                    link = getString(R.string.popular_movies) + "&" + getString(R.string.API_KEY);
                else
                    link = getString(R.string.highest_rated_movies) + "&" + getString(R.string.API_KEY);
                URL url = new URL(link);


                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                JsonStr = buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                putDataInArrays(JsonStr);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (gridItemArrayList.size() > 0) {
                gridViewAdapter.setGridData(gridItemArrayList);
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
